variable "sns_topic_arn" {
  description = "A list of ARNs (i.e. SNS Topic ARN) to notify on alerts"
  type        = list(string)
}

variable "alarm_name_prefix" {
  description = "Alarm name prefix"
  type        = string
  default     = ""
}

variable "cluster_id" {
  description = "The instance ID of the RDS database instance that you want to monitor."
  type        = list(string)
}

# CPUUtilization
variable "cpu_utilization_threshold" {
  description = "The maximum percentage of CPU utilization."
  type        = number
  default     = 80
}

variable "cpu_utilization-alarm" {
  description = "Enable Alarm to metric: cpu_utilization"
  type        = string
  default     = "true"
}

variable "cpu_utilization-comparison_operator" {
  description = "Comparison_operator to alarm"
  type        = string
  default     = "GreaterThanThreshold"
}

variable "cpu_utilization-datapoint" {
  description = "Datapoint check to alarm"
  type        = number
  default     = "1"
}

variable "cpu_utilization-period" {
  description = "Period check to alarm (in seconds)"
  type        = number
  default     = 600
}

# CPUCredtiBalance - 
variable "cpu_credit_balance_threshold" {
  description = "The minimum number of CPU credits (t2 instances only) available."
  type        = number
  default     = 20
}

variable "cpu_credit_balance-alarm" {
  description = "Enable Alarm to metric: cpu_credit_balance"
  type        = bool
  default     = true
}

variable "cpu_credit_balance-comparison_operator" {
  description = "Comparison_operator to alarm"
  type        = string
  default     = "LessThanThreshold"
}

variable "cpu_credit_balance-datapoint" {
  description = "Datapoint check to alarm"
  type        = number
  default     = 1
}

variable "cpu_credit_balance-period" {
  description = "Period check to alarm (in seconds)"
  default     = 600
}

# FreeableMemory - 
variable "freeable_memory_threshold" {
  description = "The minimum amount of available random access memory in Byte."
  type        = number
  default     = 160000000
}

variable "freeable_memory-alarm" {
  description = "Enable Alarm to metric: freeable_memory"
  type        = bool
  default     = true
}

variable "freeable_memory-comparison_operator" {
  description = "Comparison_operator to alarm"
  type        = string
  default     = "LessThanThreshold"
}

variable "freeable_memory-datapoint" {
  description = "Datapoint check to alarm"
  type        = number
  default     = 1
}

variable "freeable_memory-period" {
  description = "Period check to alarm (in seconds)"
  type        = number
  default     = 600
}

# AuroraReplicaLag
variable "aurora_replica_lag_threshold" {
  description = "The minimum amount of available random access memory in Byte."
  type        = number
  default     = 2000
}

variable "aurora_replica_lag-alarm" {
  description = "Enable Alarm to metric: aurora_replica_lag"
  type        = bool
  default     = false
}

variable "aurora_replica_lag-comparison_operator" {
  description = "Comparison_operator to alarm"
  type        = string
  default     = "LessThanThreshold"
}

variable "aurora_replica_lag-datapoint" {
  description = "Datapoint check to alarm"
  type        = number
  default     = 1
}

variable "aurora_replica_lag-period" {
  description = "Period check to alarm (in seconds)"
  type        = number
  default     = 600
}

# DatabaseConnections - 
variable "database_connections_threshold" {
  description = "The minimum amount of available random access memory in Byte."
  type        = number
  default     = 1000
}

variable "database_connections-alarm" {
  description = "Enable Alarm to metric: database_connections"
  type        = bool
  default     = true
}

variable "database_connections-comparison_operator" {
  description = "Comparison_operator to alarm"
  type        = string
  default     = "GreaterThanOrEqualToThreshold"
}

variable "database_connections-datapoint" {
  description = "Datapoint check to alarm"
  type        = number
  default     = 1
}

variable "database_connections-period" {
  description = "Period check to alarm (in seconds)"
  type        = number
  default     = 60
}

variable "tags" {
  description = "(Optional) A map of tags to assign to the all resources"
  type        = map(string)
  default     = {}
}
