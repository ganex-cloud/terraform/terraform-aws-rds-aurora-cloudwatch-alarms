data "aws_iam_account_alias" "current" {}

locals {
  alarm_name_prefix = title(var.alarm_name_prefix == "" ? data.aws_iam_account_alias.current.account_alias : var.alarm_name_prefix)

  thresholds = {
    CPUUtilizationThreshold      = min(max(var.cpu_utilization_threshold, 0), 100)
    CPUCreditBalanceThreshold    = max(var.cpu_credit_balance_threshold, 0)
    FreeableMemoryThreshold      = max(var.freeable_memory_threshold, 0)
    AuroraReplicaLagThreshold    = max(var.aurora_replica_lag_threshold, 0)
    DatabaseConnectionsThreshold = max(var.database_connections_threshold, 0)
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization" {
  count               = var.cpu_utilization-alarm ? length(var.cluster_id) : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${element(var.cluster_id, count.index)}-CPUUtilization"
  comparison_operator = var.cpu_utilization-comparison_operator
  evaluation_periods  = var.cpu_utilization-datapoint
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = var.cpu_utilization-period
  statistic           = "Average"
  threshold           = local.thresholds["CPUUtilizationThreshold"]
  alarm_description   = "Average database CPU utilization over last ${var.cpu_utilization-period} seconds too high"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = "${element(var.cluster_id, count.index)}"
  }
}

resource "aws_cloudwatch_metric_alarm" "cpu_credit_balance" {
  count               = var.cpu_credit_balance-alarm ? length(var.cluster_id) : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${element(var.cluster_id, count.index)}-CPUCreditBalance"
  comparison_operator = var.cpu_credit_balance-comparison_operator
  evaluation_periods  = var.cpu_credit_balance-datapoint
  metric_name         = "CPUCreditBalance"
  namespace           = "AWS/RDS"
  period              = var.cpu_credit_balance-period
  statistic           = "Average"
  threshold           = local.thresholds["CPUCreditBalanceThreshold"]
  alarm_description   = "Average database CPU credit balance over last ${var.cpu_credit_balance-period} seconds too low, expect a significant performance drop soon"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = "${element(var.cluster_id, count.index)}"
  }
}

resource "aws_cloudwatch_metric_alarm" "freeable_memory" {
  count               = var.freeable_memory-alarm ? length(var.cluster_id) : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${element(var.cluster_id, count.index)}-FreeableMemory"
  comparison_operator = var.freeable_memory-comparison_operator
  evaluation_periods  = var.freeable_memory-datapoint
  metric_name         = "FreeableMemory"
  namespace           = "AWS/RDS"
  period              = var.freeable_memory-period
  statistic           = "Average"
  threshold           = local.thresholds["FreeableMemoryThreshold"]
  alarm_description   = "Average database freeable memory over last ${var.freeable_memory-period} seconds too low, performance may suffer"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = "${element(var.cluster_id, count.index)}"
  }
}

resource "aws_cloudwatch_metric_alarm" "database_connections" {
  count               = var.database_connections-alarm ? length(var.cluster_id) : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${element(var.cluster_id, count.index)}-DatabaseConnections"
  comparison_operator = var.database_connections-comparison_operator
  evaluation_periods  = var.database_connections-datapoint
  metric_name         = "DatabaseConnections"
  namespace           = "AWS/RDS"
  period              = var.database_connections-period
  statistic           = "Average"
  threshold           = local.thresholds["DatabaseConnectionsThreshold"]
  alarm_description   = "Average database freeable memory over last ${var.database_connections-period} seconds too low, performance may suffer"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = "${element(var.cluster_id, count.index)}"
  }
}

resource "aws_cloudwatch_metric_alarm" "aurora_replica_lag" {
  count               = var.aurora_replica_lag-alarm ? length(var.cluster_id) : 0
  alarm_name          = "[${title(local.alarm_name_prefix)}] rds-${element(var.cluster_id, count.index)}-AuroraReplicaLag"
  comparison_operator = var.aurora_replica_lag-comparison_operator
  evaluation_periods  = var.aurora_replica_lag-datapoint
  metric_name         = "AuroraReplicaLag"
  namespace           = "AWS/RDS"
  period              = var.aurora_replica_lag-period
  statistic           = "Average"
  threshold           = local.thresholds["AuroraReplicaLagThreshold"]
  alarm_description   = "Average database freeable memory over last ${var.aurora_replica_lag-period} seconds too low, performance may suffer"
  alarm_actions       = var.sns_topic_arn
  ok_actions          = var.sns_topic_arn
  tags                = var.tags

  dimensions = {
    DBInstanceIdentifier = "${element(var.cluster_id, count.index)}"
  }
}

